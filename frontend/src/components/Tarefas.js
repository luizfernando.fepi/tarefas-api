import React, { Component } from 'react';
import { Button, Form, Modal, Table } from 'react-bootstrap';
import './Cadastros.css';

export class Tarefas extends Component {
  constructor(props) {
    super(props);

    this.state = {
      id: 0,
      descricao: '',
      prioridade: 0,
      modalAberta: false,
      tarefas: []
    };

    this.submit = this.submit.bind(this);
    this.handleDescricaoChange = this.handleDescricaoChange.bind(this);
    this.handlePrioridadeChange = this.handlePrioridadeChange.bind(this);
    this.cancelar = this.cancelar.bind(this);
    this.fecharModal = this.fecharModal.bind(this);
    this.abrirModalInserir = this.abrirModalInserir.bind(this);
    this.abrirModalAtualizar = this.abrirModalAtualizar.bind(this);
  }

  componentWillMount() {
    this.buscarTarefas();
  }

  buscarTarefas() {
    fetch('/api/tarefas')
      .then(response => response.json())
      .then(data => this.setState({ tarefas: data }));
  }

  buscarTarefa(id) {
    fetch('/api/tarefas/' + id)
      .then(response => response.json())
      .then(data => this.setState(
        {
          id: data.id,
          descricao: data.descricao,
          prioridade: data.prioridade
        }));
  }
  
  inserirTarefa(tarefa) {
    fetch('/api/tarefas', {
      method: 'post',
      headers: {'Content-Type':'application/json'},
      body: JSON.stringify(tarefa)
    }).then((resposta) => {
      if (resposta.ok) {
        this.buscarTarefas();
        this.cancelar();
      } else {
        alert(JSON.stringify(resposta));
      }
    });
  }

  atualizarTarefa(tarefa) {
    fetch('/api/tarefas/' + tarefa.id, {
      method: 'put',
      headers: {'Content-Type':'application/json'},
      body: JSON.stringify(tarefa)
    }).then((resposta) => {
      if (resposta.ok) {
        this.buscarTarefas();
        this.cancelar();
      } else {
        alert(JSON.stringify(resposta));
      }
    });
  }

  excluirTarefa(id) {
    fetch('/api/tarefas/' + id, {
      method: 'delete'
    }).then((resposta) => {
      if (resposta.ok) {
        this.buscarTarefas();
        this.cancelar();
      } else {
        alert(JSON.stringify(resposta));
      }
    });
  }

  submit(e) {
    e.preventDefault();

    const tarefa = {
      id: this.state.id,
      descricao: this.state.descricao,
      prioridade: this.state.prioridade
    };

    if (this.state.id === 0) {
      this.inserirTarefa(tarefa);
    } else {
      this.atualizarTarefa(tarefa);
    }
  }

  handleDescricaoChange(e) {
    this.setState({
      descricao: e.target.value
    });
  }

  handlePrioridadeChange(e) {
    this.setState({
      prioridade: e.target.value
    });
  }

  cancelar() {
    this.setState({
      id: 0,
      descricao: '',
      prioridade: 0,
      modalAberta: false
    });
  }

  fecharModal() {
    this.setState({
      modalAberta: false
    })
  }

  abrirModalInserir() {
    this.setState({
      modalAberta: true
    })
  }

  abrirModalAtualizar(id) {
    this.setState({
      id: id,
      modalAberta: true
    });

    this.buscarTarefa(id);
  }

  renderTabela() {
    let colunasTabela = [];
    for (var i = 0; i < this.state.tarefas.length; i++) {
      const tarefa = this.state.tarefas[i];
      const coluna = (
      <tr key={tarefa.id}>
        <td>{tarefa.descricao}</td>
        <td>{tarefa.prioridade}</td>
        
        <td>
          <div>
            <Button variant="link" onClick={() => this.abrirModalAtualizar(tarefa.id)}>Atualizar</Button>
            <Button variant="link" onClick={() => this.excluirTarefa(tarefa.id)}>Excluir</Button>
          </div>
        </td>
      </tr>);
      colunasTabela.push(coluna);
    }

    return (
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Descrição</th>
            <th>Prioridade</th>
            <th>Status</th>
            <th>Porcentagem concluída</th>
            <th>Data de Conclusão</th>
            <th>Ações</th>
          </tr>
        </thead>
        <tbody>
          {colunasTabela}
        </tbody>
      </Table>
    );
  }

  renderModal() {
    return (
      <Modal show={this.state.modalAberta} onHide={this.fecharModal}>
        <Modal.Header closeButton>
          <Modal.Title>Inserir novo item</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form id="modalForm" onSubmit={this.submit}>
            <Form.Group>
              <Form.Label>Descrição</Form.Label>
              <Form.Control type='text' placeholder='Descrição da tarefa' value={this.state.descricao} onChange={this.handleDescricaoChange} />
            </Form.Group>
            <Form.Group>
              <Form.Label>Prioridade</Form.Label>
              <Form.Control type='number' placeholder='Prioridade' value={this.state.prioridade} onChange={this.handlePrioridadeChange} />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.cancelar}>
            Cancelar
          </Button>
          <Button variant="primary" form="modalForm" type="submit">
            Confirmar
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }

  render() {
    return (
      <div>
        <Button variant="primary" className="button-novo" onClick={this.abrirModalInserir}>Novo item</Button>
        {this.renderTabela()}
        {this.renderModal()}
      </div>
    );
  }
}
