import React, { Component } from 'react';

export class Home extends Component {
  render() {
    return (
      <div>
        <h1>Bem vindo à aplicação Nome da Aplicação</h1>
        <p>Esta é uma aplicação de testes desenvolvida durante a disciplina Mobile Web Development utilizando as seguintes tecnologias:</p>
        <ul>
            <li>.Net Core Web API</li>
            <li>ReactJS</li>
            <li>PostgreSQL</li>
            <li>Docker</li>
        </ul>
      </div>
    );
  }
}