﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TarefaApi.Model
{
  public class TarefaResponsavel
  {
    public int TarefaId { get; set; }

    public Tarefa Tarefa { get; set; }

    public int ResponsavelId { get; set; }

    public Responsavel Responsavel { get; set; }
  }


}
