﻿using System.Collections.Generic;

namespace TarefaApi.Model
{
  public class Responsavel
  {
    public int Id { get; set; }

    public string Nome { get; set; }

    public ICollection<TarefaResponsavel> TarefaResponsaveis { get; set; }
  }
}