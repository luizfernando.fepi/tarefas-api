﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace TarefaApi.Model
{
  public class Execucao
  {
    public int Id { get; set; }

    public int PorcentagemConcluida { get; set; }

    // A forma padrão de serializar um enum é retornando o valor inteiro dele.
    // Esse atributo permite que ele seja serializado como string. Coloquei aqui
    // apenas para facilitar a leitura durante o retorno... :)
    [JsonConverter(typeof(StringEnumConverter))]
    public Status Status { get; set; }

    public DateTime? DataConclusao { get; set; }

    [JsonIgnore]
    public int TarefaId { get; set; }

    // O atributo JsonIgnore faz com que essa propriedade não
    // seja utilizada ao converter o objeto em Json para retornar no serviço.
    // Isso é importante para não gerar uma dependência circular. Ex.:
    // Converto uma tarefa em Json. A tarefa tem uma Execucao que tem uma tarefa,
    // que tem uma execucao, etc, etc
    [JsonIgnore]
    public Tarefa Tarefa { get; set; }
  }

  public enum Status
  {
    NAO_INICIADO,
    EM_ANDAMENTO,
    CONCLUIDO
  }
}