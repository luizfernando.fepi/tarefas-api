﻿namespace TarefaApi.Model
{
  public class Arquivo
  {
    public int Id { get; set; }

    public string Caminho { get; set; }
  }
}