using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TarefaApi.Model;

namespace tarefas_api.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class TarefasController : ControllerBase
  {
    private readonly ApiDbContext _context;

    public TarefasController(ApiDbContext context)
    {
      _context = context;
    }

    // GET: api/Tarefas
    [HttpGet]
    public ActionResult<IEnumerable<Tarefa>> Get()
    {
      return _context.Tarefas
        .Include(tarefa => tarefa.Execucao)
        .Include(tarefa => tarefa.Arquivos)
        .Include(tarefa => tarefa.TarefaResponsaveis)
        .ToList();
    }

    // GET: api/Tarefas/5
    [HttpGet("{id}")]
    public ActionResult<Tarefa> GetById(int id)
    {
      var item = _context.Tarefas.Find(id);
      _context.Entry(item).Reference(tarefa => tarefa.Execucao).Load();
      _context.Entry(item).Collection(tarefa => tarefa.Arquivos).Load();
      _context.Entry(item).Collection(tarefa => tarefa.TarefaResponsaveis).Load();

      if (item == null)
      {
        return NotFound();
      }

      return item;
    }

    // POST: api/Tarefas
    [HttpPost]
    public ActionResult<Tarefa> Post(TarefaViewModel item)
    {
      var tarefa = new Tarefa()
      {
        Descricao = item.Descricao,
        Prioridade = item.Prioridade,
        Execucao = new Execucao()
        {
          PorcentagemConcluida = 0,
          Status = Status.NAO_INICIADO
        }
      };
      _context.Tarefas.Add(tarefa);
      _context.SaveChanges();

      return CreatedAtAction(nameof(GetById), new { id = item.Id }, item);
    }

    // PUT: api/Tarefas/5
    [HttpPut("{id}")]
    public IActionResult Put(int id, TarefaViewModel item)
    {
      if (id != item.Id)
      {
        return BadRequest();
      }

      var tarefa = _context.Tarefas.Find(item.Id);
      tarefa.Descricao = item.Descricao;
      tarefa.Prioridade = item.Prioridade;
      _context.Entry(tarefa).State = EntityState.Modified;
      _context.SaveChanges();

      return NoContent();
    }

    // DELETE: api/Tarefas/5
    [HttpDelete("{id}")]
    public IActionResult Delete(int id)
    {
      var item = _context.Tarefas.Find(id);

      if (item == null)
      {
        return NotFound();
      }

      _context.Tarefas.Remove(item);
      _context.SaveChangesAsync();

      return NoContent();
    }
  }
}
