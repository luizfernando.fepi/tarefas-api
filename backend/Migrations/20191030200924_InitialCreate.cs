﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace tarefas_api.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TarefaId",
                table: "Responsavel",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Responsavel_TarefaId",
                table: "Responsavel",
                column: "TarefaId");

            migrationBuilder.AddForeignKey(
                name: "FK_Responsavel_Tarefas_TarefaId",
                table: "Responsavel",
                column: "TarefaId",
                principalTable: "Tarefas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Responsavel_Tarefas_TarefaId",
                table: "Responsavel");

            migrationBuilder.DropIndex(
                name: "IX_Responsavel_TarefaId",
                table: "Responsavel");

            migrationBuilder.DropColumn(
                name: "TarefaId",
                table: "Responsavel");
        }
    }
}
